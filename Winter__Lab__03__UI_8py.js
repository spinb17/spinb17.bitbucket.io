var Winter__Lab__03__UI_8py =
[
    [ "change", "Winter__Lab__03__UI_8py.html#a36fbef02a96aecae715405975d642d04", null ],
    [ "char_check", "Winter__Lab__03__UI_8py.html#a32e6869d1e121507a345c80f62bc289d", null ],
    [ "data_start", "Winter__Lab__03__UI_8py.html#a178021257eacde4205eb8a8475d7b417", null ],
    [ "data_stop", "Winter__Lab__03__UI_8py.html#a6445a9f13489d6e2701488f4be2025a9", null ],
    [ "delimiter", "Winter__Lab__03__UI_8py.html#a573fbd74641251617453eb051e5831c9", null ],
    [ "init_t", "Winter__Lab__03__UI_8py.html#a1c222d083876e98d87f98372499bb6ea", null ],
    [ "motor_pos", "Winter__Lab__03__UI_8py.html#af3071860e0f7e4f93f5087469edc1006", null ],
    [ "n", "Winter__Lab__03__UI_8py.html#a3cba26fb2cfd0ab67d857577407edcd9", null ],
    [ "run", "Winter__Lab__03__UI_8py.html#a24575148b118f60734352e1940801d2d", null ],
    [ "ser", "Winter__Lab__03__UI_8py.html#a00e46b667f2aeed2a9fec75bee3ee774", null ],
    [ "t", "Winter__Lab__03__UI_8py.html#ae10d3153099f1bd1457d71bb738dda17", null ],
    [ "value", "Winter__Lab__03__UI_8py.html#ab445bcfbdd26172f9e298dd0ab5eba5d", null ],
    [ "values", "Winter__Lab__03__UI_8py.html#afa2020ec8bd0387236c51703679611dc", null ]
];