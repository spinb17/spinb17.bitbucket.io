var files_dup =
[
    [ "cotask.py", "cotask_8py.html", "cotask_8py" ],
    [ "Encoder.py", "Encoder_8py.html", [
      [ "Encoder", "classEncoder_1_1Encoder.html", "classEncoder_1_1Encoder" ]
    ] ],
    [ "main_0x04.py", "main__0x04_8py.html", "main__0x04_8py" ],
    [ "mcp9808.py", "mcp9808_8py.html", "mcp9808_8py" ],
    [ "Motor_Driver.py", "Motor__Driver_8py.html", "Motor__Driver_8py" ],
    [ "PrelimCalcs.py", "PrelimCalcs_8py.html", null ],
    [ "proj.py", "proj_8py.html", "proj_8py" ],
    [ "Simulation.py", "Simulation_8py.html", null ],
    [ "task_share.py", "task__share_8py.html", "task__share_8py" ],
    [ "TouchPad_Driver.py", "TouchPad__Driver_8py.html", "TouchPad__Driver_8py" ],
    [ "Winter_Lab_01.py", "Winter__Lab__01_8py.html", "Winter__Lab__01_8py" ],
    [ "Winter_Lab_02.py", "Winter__Lab__02_8py.html", "Winter__Lab__02_8py" ],
    [ "Winter_Lab_03_main.py", "Winter__Lab__03__main_8py.html", "Winter__Lab__03__main_8py" ],
    [ "Winter_Lab_03_UI.py", "Winter__Lab__03__UI_8py.html", "Winter__Lab__03__UI_8py" ]
];