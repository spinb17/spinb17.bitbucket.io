/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Mechatronics", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Stand Alone Labs", "page1.html", [
      [ "Lab01", "page1.html#sec_lab1", null ],
      [ "Lab02", "page1.html#sec_lab2", null ],
      [ "Lab03", "page1.html#sec_lab3", null ],
      [ "Lab04", "page1.html#sec_lab4", null ]
    ] ],
    [ "Term Project", "page2.html", [
      [ "Term Project Introduction", "page2.html#Term_Intro", null ],
      [ "Preliminary Calculations", "page2.html#Term_sec1", null ],
      [ "Simulation of System", "page2.html#Term_sec2", null ],
      [ "Touch Pad Driver", "page2.html#Term_sec3", null ],
      [ "Motor Driver and Encoder", "page2.html#Term_Sec4", null ],
      [ "Term Project", "page2.html#Term_Sec5", null ]
    ] ],
    [ "Simulation of System", "page3.html", null ],
    [ "Simulation K Calcs", "page4.html", null ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
""
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';